local wwiseSDK = os.getenv("WWISESDK")

package.path = package.path .. ";" .. path.getabsolute(wwiseSDK .. "/source/Build") .. "/?.lua" .. ';'.. path.getabsolute(wwiseSDK .. "/../Scripts/Build") .. "/?.lua"

require "PremakePlatforms"
require "Premake5Helper"
require "Premake5PathHelpers"

_AK_ROOT_DIR = path.getabsolute(wwiseSDK .. "/source/Build") .. "/"

local platform = AK.Platform

function SoundEngineInternals(suffix, communication)
	local d = {
		"AkMusicEngine"..suffix,
		"AkSpatialAudio"..suffix,
		"AkMemoryMgr"..suffix,
		"AkStreamMgr"..suffix,
	}

	if communication then
		d[#d+1] = "CommunicationCentral"..suffix
	end

	d[#d+1] = "AkSoundEngine"..suffix
	d[#d+1] = "AkSilenceSource"..suffix
	d[#d+1] = "AkSineSource"..suffix
	d[#d+1] = "AkToneSource"..suffix
	d[#d+1] = "AkSynthOneSource"..suffix
	d[#d+1] = "AkVorbisDecoder"..suffix
	d[#d+1] = "AkOpusDecoder"..suffix
	d[#d+1] = "AkAudioInputSource"..suffix

	return d
end

function Dependencies(suffix, communication)
	local d = SoundEngineInternals(suffix, communication)

	d[#d+1] = "AkCompressorFX"..suffix
	d[#d+1] = "AkDelayFX"..suffix
	d[#d+1] = "AkExpanderFX"..suffix
	d[#d+1] = "AkFlangerFX"..suffix
	d[#d+1] = "AkGainFX"..suffix
	d[#d+1] = "AkGuitarDistortionFX"..suffix
	d[#d+1] = "AkHarmonizerFX"..suffix
	d[#d+1] = "AkMatrixReverbFX"..suffix
	d[#d+1] = "AkMeterFX"..suffix
	d[#d+1] = "AkParametricEQFX"..suffix
	d[#d+1] = "AkPeakLimiterFX"..suffix
	d[#d+1] = "AkPitchShifterFX"..suffix
	d[#d+1] = "AkRecorderFX"..suffix
	d[#d+1] = "AkReflectFX"..suffix
	d[#d+1] = "AkRoomVerbFX"..suffix
	d[#d+1] = "AkStereoDelayFX"..suffix
	d[#d+1] = "AkTimeStretchFX"..suffix
	d[#d+1] = "AkTremoloFX"..suffix

	return d
end

local function ConvertDependenciesToPostBuildStep(dependencies)
	local addLogs = true

	local newLine = "\r\n"
	local saveRootDirectory = "pushd ."
	local returnToRootDirectory = "popd"
	local ar = "$(NINTENDO_SDK_ROOT)/Compilers/NX/nx/aarch64/bin/aarch64-nintendo-nx-elf-ar.exe"

	local output = ""

	if addLogs then
		output = output .. "echo This script is generated within the premake5.lua file." .. newLine
	end

	for i,v in ipairs(dependencies) do
		local tempDirectory = "$(IntDir)" .. v

		local currentLibPath = "$(WWISESDK)/$(Platform)/$(Configuration)/lib/lib" .. v .. ".a"

		local createDirectory = 
			"mkdir " .. tempDirectory .. newLine ..
			"cd " .. tempDirectory

		local extract = ar .. " x \"" .. currentLibPath .. "\""

		local addObjectsToArchive = ar .. " r $(TargetPath) " .. tempDirectory .. "/*.o"

		local removeDirectory = "rmdir /s /q " .. tempDirectory

		if addLogs then
			extract = "echo Extracting objects from library \"" .. currentLibPath .. "\" into temporary directory \"" .. tempDirectory .. "\"." .. newLine .. extract
			addObjectsToArchive = "echo Adding objects to library \"$(TargetFileName)\"." .. newLine .. addObjectsToArchive
		end

		output = output
			.. saveRootDirectory .. newLine
			.. createDirectory .. newLine
			.. extract .. newLine
			.. returnToRootDirectory .. newLine
			.. addObjectsToArchive .. newLine
			.. removeDirectory .. newLine
	end

	return output
end

function SetupTargetAndLibDir(platformName)
	if platformName == "Mac" or platformName == "iOS" or platformName == "tvOS" then
		xcodebuildsettings {
			WWISESDK = wwiseSDK
		}
	end

	local baseTargetDir = "../../Integration/Assets/Wwise/Deployment/Plugins/"

	targetdir(baseTargetDir .. platformName .. "/%{cfg.architecture}/%{cfg.buildcfg}")

	if platformName == "Windows" then
		libdirs("$(WWISESDK)/%{cfg.platform}" .. GetSuffixFromCurrentAction() .. "/%{cfg.buildcfg}(StaticCRT)/lib")
	elseif platformName == "WSA" then
		targetdir(baseTargetDir .. "WSA/WSA_UWP_%{cfg.platform}/%{cfg.buildcfg}")
		libdirs("$(WWISESDK)/UWP_%{cfg.platform}" .. GetSuffixFromCurrentAction() .. "/%{cfg.buildcfg}/lib")
	elseif platformName == "XboxOne" then
		targetdir(baseTargetDir .. "XboxOne/%{cfg.buildcfg}")
		libdirs("$(WWISESDK)/XboxOne" .. GetSuffixFromCurrentAction() .. "/%{cfg.buildcfg}/lib")
	elseif platformName == "PS4" then
		targetdir(baseTargetDir .. "PS4/%{cfg.buildcfg}")
		libdirs("$(WWISESDK)/PS4/%{cfg.buildcfg}/lib")
	elseif platformName == "Switch" then
		targetdir(baseTargetDir .. "Switch/%{cfg.platform}/%{cfg.buildcfg}")
		libdirs("$(WWISESDK)/%{cfg.platform}/%{cfg.buildcfg}/lib")
	elseif platformName == "Lumin" then
		targetdir(baseTargetDir .. "Lumin/%{cfg.buildcfg}")
		libdirs("$(WWISESDK)/Lumin/%{cfg.buildcfg}/lib")
	elseif platformName == "Linux" then
		libdirs("$(WWISESDK)/Linux_%{cfg.platform}/%{cfg.buildcfg}/lib")
	elseif platformName == "Stadia" then
		targetdir(baseTargetDir .. platformName .. "/%{cfg.buildcfg}")
		libdirs("$(WWISESDK)/GGP/%{cfg.buildcfg}/lib")
	elseif platformName == "Mac" then
		targetdir(baseTargetDir .. platformName .. "/%{cfg.buildcfg}")
		libdirs("${WWISESDK}/" .. platformName .. "/%{cfg.buildcfg}/lib")
	elseif platformName == "iOS" or platformName == "tvOS" then
		targetdir(baseTargetDir .. platformName .. "/%{cfg.buildcfg}")
		libdirs("${WWISESDK}/" .. platformName .. "/$(CONFIGURATION)$(EFFECTIVE_PLATFORM_NAME)/lib/")
	else
		libdirs("$(WWISESDK)/%{cfg.platform}_%{cfg.architecture}/%{cfg.buildcfg}/lib")
	end
end

function CreateCommonProject(platformName, suffix)
	-- A project defines one build target
	local pfFolder = "../"..platformName.."/";
	local prj = AK.Platform.CreateProject("AkSoundEngine" .. platformName, "AkSoundEngine" .. platformName, pfFolder, GetSuffixFromCurrentAction(), nil, "SharedLib")
		targetname "AkSoundEngine"
		-- Files must be explicitly specified (no wildcards) because our build system calls premake before copying the sample files
		files {
			"../Common/AkCallbackSerializer.cpp",
			pfFolder .. "AkDefaultIOHookBlocking.cpp",
			"../Common/AkFileLocationBase.cpp",
			"../Common/AkMultipleFileLocation.cpp",
			"../Common/AkFilePackage.cpp",
			"../Common/AkFilePackageLUT.cpp",
			"../Common/AkSoundEngineStubs.cpp",
			pfFolder .. "SoundEngine_wrap.cxx",
			pfFolder .. "stdafx.cpp",
		}

		defines {
			"AUDIOKINETIC",
		}

		includedirs {
			"../"..platformName,
			"$(WWISESDK)/include",
			"."
		}

		if platformName == "Mac" then
			sharedlibtype "OSXBundle"
		end

		SetupTargetAndLibDir(platformName)

		configuration "Debug"
			defines { "_DEBUG" }
			symbols "On"
			links(Dependencies(suffix, true))

		configuration "Profile"
			defines { "NDEBUG" }
			optimize "On"
			links(Dependencies(suffix, true))

		configuration "Release"
			defines { "NDEBUG", "AK_OPTIMIZED" }
			optimize "On"
			links(Dependencies(suffix, false))

		configuration "*"
		if platformName == "Windows" then
			links {
				"dxguid"
			}
	
			filter "Debug* or Profile*"
				links { "ws2_32" }
		elseif platformName == "Android" then
			files
			{
				"../Android/AkFileHelpers.cpp",
			}
	
			excludes
			{
				"../Common/AkMultipleFileLocation.cpp",
			}
	
			includedirs
			{
				"$(WWISESDK)/samples/SoundEngine/Android/libzip/lib",
			}
	
			links
			{
				"OpenSLES",
				"android",
				"log",
				"dl",
				"z",
				"zip",
				"stdc++"
			}
	
			linkoptions { "-L$(WWISESDK)/Android_$(TARGET_ARCH_ABI)/$(CONFIGURATION)/lib/" }
	
			-- Enable symbol stripping on all targets. ndk-build generate a non-stripped version of the library in its out folder
			configuration("*")
				symbols "Off"
		elseif platformName == "PS4" then
			links {
				"SceFios2_stub_weak",
				"SceAudioOut_stub_weak",
				"ScePosix_stub_weak",
				"SceAjm_stub_weak",
			}
	
			filter "Debug* or Profile*"
				links {
					"SceNet_stub_weak",
					"SceNetCtl_stub_weak",
				}
		elseif platformName == "XboxOne" then
			links {
				"acphal",
				"kernelx",
				"MMDevApi"
			}
	
			linkoptions {
				"/WINMD:NO"
			}

			filter "Debug* or Profile*"
				links {
					"ws2_32",
				}
		elseif platformName == "Switch" then
			kind "StaticLib"
			targetname "AkSoundEngineWrapper"
	
			local function SwitchAddPostBuildCommand(useCommunication)
				local depends = SoundEngineInternals("", useCommunication)
				depends[#depends+1] = "AkOpusNXDecoder"
	
				postbuildcommands(ConvertDependenciesToPostBuildStep(depends))
			end
	
			filter "Debug* or Profile*"
				SwitchAddPostBuildCommand(true)
			filter "Release*"
				SwitchAddPostBuildCommand(false)
			
			
			-- Need to set the propsheets that way because the Wwise premake scripts insert a relative path.
			-- Since the Wwise SDK can be installed anywhere on a customer's machine, we need to use the
			-- environment variable instead. Simply using vs_propsheet() does not work, because it appends 
			-- to the internal array. We need to actually get to the internal array, and set it to what we
			-- need.
			for cfg in premake.project.eachconfig(prj) do
				if string.match(cfg.name, "Debug") then
					strConfig = "Debug"
				elseif string.match(cfg.name, "Profile") then 
					strConfig = "Develop"
				else
					strConfig = "Release"
				end
				
				cfg.vs_propsheet = {
					"$(WWISESDK)/source/Build/PropertySheets/NX/NintendoSdkSpec_NX.props",
					"$(WWISESDK)/source/Build/PropertySheets/NX/NintendoSdkVcProjectSettings.props",
					"$(WWISESDK)/source/Build/PropertySheets/NX/NintendoSdkBuildType_".. strConfig .. ".props"
				}
			end
				
		elseif platformName == "Lumin" then
			links {
				"log",
				"ml_audio"
			}
	
			libdirs {
				"$(MLSDK)/lib/lumin",
			}
		elseif platformName == "Linux" then
			links {
				"SDL2"
			}
			
			filter "Profile* or Release*"
				postbuildcommands ("strip " .. "%{cfg.buildtarget.abspath}")

			configuration "*x64"
				buildoptions { "-m64" }
		elseif platformName == "Stadia" then
			filter "Profile* or Release*"
				postbuildcommands ("strip " .. "%{cfg.buildtarget.abspath}")
		elseif platformName == "Mac" then
			links {
				"AudioToolbox.framework",
				"AudioUnit.framework",
				"Foundation.framework",
				"CoreAudio.framework",
				"AkAACDecoder"
			}
		elseif platformName == "iOS" or platformName == "tvOS" then
			kind "StaticLib"
			xcodebuildsettings {
				OTHER_LIBTOOLFLAGS = "$(OTHER_LDFLAGS)",
				SEPARATE_STRIP = "NO",
				LINK_WITH_STANDARD_LIBRARIES = "NO"
			}
			links { 
				"Foundation.framework",
				"UIKit.framework",
				"AVFoundation.framework",
				"AudioToolbox.framework",
				"CoreAudio.framework",
				"AkAACDecoder"
			}
		end
	return prj
end

function CreateWaapiProject(platformName, suffix)
	-- A project defines one build target
	local pfFolder = "../"..platformName.."/";
	local prj = AK.Platform.CreateProject("AkWaapiClient", "AkWaapiClient", pfFolder, GetSuffixFromCurrentAction(), nil, "SharedLib")
		-- Files must be explicitly specified (no wildcards) because our build system calls premake before copying the sample files
		files {
			"../Common/AkWaapiClientStubs.cpp"
			}

		includedirs {
			"$(WWISESDK)/include",
			"."
			}

		links("AkAutobahn")

		cppdialect "C++11"

		if platformName == "Mac" then
			sharedlibtype "OSXBundle"
		end

		SetupTargetAndLibDir(platformName)

		configuration "Debug"
			buildoptions { "/MTd" }
			defines { "_DEBUG" }
			symbols "On"

		configuration "Profile"
			buildoptions { "/MT" }
			defines { "NDEBUG" }
			optimize "On"

		configuration "Release"
			buildoptions { "/MT" }
			defines { "NDEBUG", "AK_OPTIMIZED" }
			optimize "On"

		configuration "*"
	return prj
end

-- This is the main() of the script
if table.find(platform.validActions, _ACTION) then
	local unityPlatformName = platform.name

	if platform.name == "UWP" then
		unityPlatformName = "WSA"
	elseif platform.name == "NX" then
		unityPlatformName = "Switch"
	elseif platform.name == "GGP" then
		unityPlatformName = "Stadia"
	end

	solution("AkSoundEngine" .. unityPlatformName)
	configurations { "Debug", "Profile", "Release" }

	if unityPlatformName == "Android" then
		location("../Android/jni/")
	else
		location("../" .. unityPlatformName)
	end

	local cfgplatforms = AK.Platform.platforms
	if type(cfgplatforms) == 'function' then
		cfgplatforms()
	else
		platforms(cfgplatforms)

		-- mlarouche: Workaround AK.Platforms not specifying platforms and architecture for Linux
		if unityPlatformName == "Linux" then
			platforms { "x64" }

			filter "platforms:x64"
				architecture "x86_64"
		end
	end

	CreateCommonProject(unityPlatformName, "")

	if unityPlatformName == "Windows" or unityPlatformName == "Mac" then
		CreateWaapiProject(unityPlatformName, "")
	end
else
	if _ACTION ~= nil then
		premake.error(string.format("No valid action specified for platform %s, valid actions are: %s", platform.name, table.concat(platform.validActions, ",")))
	end
end
