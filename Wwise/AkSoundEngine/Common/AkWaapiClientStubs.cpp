#include <AK/WwiseAuthoringAPI/AkAutobahn/Client.h>
#include <AK/WwiseAuthoringAPI/AkAutobahn/JsonProvider.h>
#include <AK/Tools/Common/AkLock.h>
#include <AK/Tools/Common/AkAutoLock.h>
#include <AK/Tools/Common/AkPlatformFuncs.h>

#if defined( AK_ENABLE_ASSERTS )
void AkUnityAssertHook(
	const char * in_pszExpression,	///< Expression
	const char * in_pszFileName,	///< File Name
	int in_lineNumber				///< Line Number
)
{
	size_t msgLen = strlen(in_pszExpression) + strlen(in_pszFileName) + 128;
	char* msg = (char*)AkAlloca(msgLen);
#if defined(AK_ANDROID) || defined(AK_MAC_OS_X) || defined(AK_IOS) || defined(AK_LINUX) || defined(AK_NX)
	sprintf(msg, "AKASSERT: %s. File: %s, line: %d", in_pszExpression, in_pszFileName, in_lineNumber);
#else
	sprintf_s(msg, msgLen, "AKASSERT: %s. File: %s, line: %d", in_pszExpression, in_pszFileName, in_lineNumber);
#endif
	AKPLATFORM::OutputDebugMsg(msg);
}

AkAssertHook g_pAssertHook = AkUnityAssertHook;
#endif

#if defined AK_WIN || defined AK_XBOXONE
#define AK_STDCALL __stdcall
#else
#define AK_STDCALL
#endif

#ifndef AK_EXPORT
# if defined(_WIN32) || defined(__WIN32__) || defined(__CYGWIN__)
#     define AK_EXPORT __declspec(dllexport)
# else
#   if defined(__GNUC__) && defined(GCC_HASCLASSVISIBILITY)
#     define AK_EXPORT __attribute__ ((visibility("default")))
#   else
#     define AK_EXPORT
#   endif
# endif
#endif

struct PendingCallback
{
	uint64_t SubscriptionID;
	std::string JsonString;

	PendingCallback(uint64_t SubId, const std::string& Json)
		: SubscriptionID(SubId)
	{
		JsonString.append(Json);
	}
};


AK::WwiseAuthoringAPI::Client g_Client;
std::string g_LastString;
CAkLock g_PendingCallbacksLock;
std::vector<PendingCallback*> g_PendingCallbacks;

typedef void (AK_STDCALL * WampEventCallbackString)(uint64_t, const char*);
WampEventCallbackString ExternalWampEventCallback = nullptr;

void WampEventCallback(const uint64_t& in_subscriptionId, const AK::WwiseAuthoringAPI::JsonProvider& in_rJson)
{
	AkAutoLock<CAkLock> ScopedLock(g_PendingCallbacksLock);
	g_PendingCallbacks.push_back(new PendingCallback(in_subscriptionId, in_rJson.GetJsonString()));
}

#define WAAPI_CALL_HANDLE_STRING(func) \
	bool result = (func); \
	out_resultLength = (int)g_LastString.length() + 1; \
	return result


extern "C" {
	AK_EXPORT void AK_STDCALL SetWampEventCallback(WampEventCallbackString callback)
	{
		ExternalWampEventCallback = callback;
	}

	AK_EXPORT unsigned int AK_STDCALL Connect(const char* in_uri, unsigned int in_port)
	{
		return (unsigned int)g_Client.Connect(in_uri, in_port);
	}

	AK_EXPORT unsigned int AK_STDCALL IsConnected()
	{
		return (unsigned int)g_Client.IsConnected();
	}

	AK_EXPORT void AK_STDCALL Disconnect()
	{
		g_Client.Disconnect();
	}

	AK_EXPORT unsigned int AK_STDCALL Subscribe(
		const char* in_uri,
		const char* in_options,
		uint64_t& out_subscriptionId,
		int& out_resultLength)
	{
		WAAPI_CALL_HANDLE_STRING(g_Client.Subscribe(in_uri, in_options, &WampEventCallback, out_subscriptionId, g_LastString, -1));
	}

	AK_EXPORT unsigned int AK_STDCALL SubscribeWithTimeout(
		const char* in_uri,
		const char* in_options,
		uint64_t& out_subscriptionId,
		int in_timeoutMs,
		int& out_resultLength)
	{
		WAAPI_CALL_HANDLE_STRING(g_Client.Subscribe(in_uri, in_options, &WampEventCallback, out_subscriptionId, g_LastString, in_timeoutMs));
	}

	AK_EXPORT unsigned int AK_STDCALL Unsubscribe(uint64_t in_subscriptionId, int& out_resultLength)
	{
		WAAPI_CALL_HANDLE_STRING(g_Client.Unsubscribe(in_subscriptionId, g_LastString));
	}

	AK_EXPORT unsigned int AK_STDCALL UnsubscribeWithTimeout(uint64_t in_subscriptionId, int in_timeoutMs, int& out_resultLength)
	{
		WAAPI_CALL_HANDLE_STRING(g_Client.Unsubscribe(in_subscriptionId, g_LastString, in_timeoutMs));
	}


	AK_EXPORT unsigned int AK_STDCALL Call(const char* in_uri, const char* in_args, const char* in_options, int& out_resultLength)
	{
		WAAPI_CALL_HANDLE_STRING(g_Client.Call(in_uri, in_args, in_options, g_LastString));
	}

	AK_EXPORT unsigned int AK_STDCALL CallWithTimeout(const char* in_uri, const char* in_args, const char* in_options, int in_timeoutMs, int& out_resultLength)
	{
		WAAPI_CALL_HANDLE_STRING(g_Client.Call(in_uri, in_args, in_options, g_LastString, in_timeoutMs));
	}

	AK_EXPORT unsigned int AK_STDCALL GetLastString(char* resultString, int resultLength)
	{
		if (g_LastString.length() <= (std::string::size_type)resultLength)
		{
			strcpy(resultString, g_LastString.c_str());
			return true;
		}

		return false;
	}


	AK_EXPORT void AK_STDCALL ProcessCallbacks()
	{
		if (!g_PendingCallbacks.empty())
		{
			std::vector<PendingCallback*> CallbacksCopy;
			{
				AkAutoLock<CAkLock> ScopedLock(g_PendingCallbacksLock);
				CallbacksCopy = g_PendingCallbacks;
				g_PendingCallbacks.clear();
			}

			for (auto PendingCallback : CallbacksCopy)
			{
				if (ExternalWampEventCallback && PendingCallback)
				{
					ExternalWampEventCallback(PendingCallback->SubscriptionID, PendingCallback->JsonString.c_str());
				}

				delete PendingCallback;
			}

			CallbacksCopy.clear();
		}
	}
}
