#!/bin/sh
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    "$WWISEROOT/Tools/Linux/bin/premake5" --scripts="$WWISEROOT/Scripts/Premake" $@
elif [[ "$OSTYPE" == "darwin"* ]]; then
    "$WWISEROOT/Tools/Mac/bin/premake5" --scripts="$WWISEROOT/Scripts/Premake" $@
fi