﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAudioTrigger : MonoBehaviour
{
    [SerializeField] private Collider2D collider;
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(!other.CompareTag("Player")) return;
        
        SoundAPI.SetState(SoundStateGroup.MusicState, SoundState.Boss);
        collider.enabled = false;
    }
}
