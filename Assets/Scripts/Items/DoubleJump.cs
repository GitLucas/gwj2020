﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DoubleJump : MonoBehaviour {

    private PlayerMovementController player;
    private Rigidbody2D rb;
    private bool canDoubleJump;
    private float doubleJumpDelay = 5f, nextDoubleJump = 0f; 

    void Awake () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovementController>();
        rb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
        canDoubleJump = true;
    }

    public void OnJump(InputValue value)
    {
	    if (!player.CanDoubleJump() || !canDoubleJump) return;

	    rb.AddForce(Vector2.up * player.GetJumpForce(), ForceMode2D.Impulse);
	    canDoubleJump = false;
	    
    }

    void Update() {
	    if (player.CanDoubleJump() || !(Time.time > nextDoubleJump)) return;
	    
	    nextDoubleJump = Time.time + doubleJumpDelay;
	    canDoubleJump = true;
    }
}
