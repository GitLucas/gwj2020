﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTextDisabler : MonoBehaviour
{
    [SerializeField] private Floor startingFloor;
    [SerializeField] private GameObject text;

    private void Start() => startingFloor.OnFirstLevelSurpassed += () => text.SetActive(false);
}
