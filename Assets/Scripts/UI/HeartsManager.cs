﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartsManager : MonoBehaviour
{
    [SerializeField] private Jugador player;
    [SerializeField] private Animator[] hearts;

    private int _currentHeart = 2;

    private void Start()
    {
        player.OnHealthLost += ManageHearts;
    }

    private void ManageHearts()
    {
        if(_currentHeart >= 0)
            hearts[_currentHeart--].SetTrigger("HPLost");
    }
}
