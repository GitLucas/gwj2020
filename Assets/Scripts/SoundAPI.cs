﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SoundAPI
{
    public static void PostEvent(SoundEvent evt, GameObject source) => AkSoundEngine.PostEvent((uint)evt, source);

    public static void SetState(SoundStateGroup grp, SoundState st) => AkSoundEngine.SetState((uint) grp, (uint) st);

    public static void SetParameter(uint parameter, float value, GameObject source = null) => AkSoundEngine.SetRTPCValue(parameter, value, source);

    public static void SetSwitch(string switchName, string switchValue, GameObject source) => AkSoundEngine.SetSwitch(switchName, switchValue, source);
}

public enum SoundEvent : uint 
{
    Spike = 2812137,
    StopSecretRoom = 105658911,
    Attack = 180661997,
    PlayMusic = 202194903,
    Hit = 1116398592,
    PlaySecretRoom = 1768088025,
    EnemyAttack = 1781417190,
    PlayerDeath = 2226114503,
    FlierEnemyDeath = 2240500274,
    PlayerFootstep = 2385628198,
    EnemyFootstep = 3217092839,
    EnemyDeath = 3805217580
}

public enum SoundStateGroup : uint
{
    MusicState = 1021618141,
    Player = 1069431850
}

public enum SoundState : uint
{
    General = 133642231,
    SecretRoom = 1348541312,
    Boss = 1560169506,
    Dead = 2044049779,
    PlayerAlive = 655265632,
    PlayerDead = 2044049779
}

