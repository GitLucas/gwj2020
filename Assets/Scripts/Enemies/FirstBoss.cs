﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstBoss : HittableByPlayer, CanHitPlayer
{

    [SerializeField] private SpriteRenderer mySpriteRenderer;
    [SerializeField] private Collider2D myCollider;
    [SerializeField] private Animator myAnimator;
    [SerializeField] private GameObject[] attackFrames;

    private float _fireRate = 8f;
    private float _nextFire;

    private Transform _player;
    private int _currentFrame = -1;
    
    public void UpdateAttackFrame()
    {
        if (_currentFrame < 0)
            _currentFrame = 0;
        else
            attackFrames[_currentFrame++].SetActive(false);

        if (_currentFrame >= attackFrames.Length) 
            _currentFrame = -1;
        else 
            attackFrames[_currentFrame].SetActive(true);

    }

    public override void ReceiveDamage(int dmg)
    {
        base.ReceiveDamage(dmg);
        
        if(currentHealth <= 0)
            StartCoroutine(EndGame());
        else
        {
            myAnimator.SetTrigger("Hurt");
        }

        IEnumerator EndGame()
        {
            Time.timeScale = 0;
            yield return new WaitForSecondsRealtime(2f);
            mySpriteRenderer.enabled = false;
            yield return new WaitForSecondsRealtime(.75f);
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }


    public float GetDamage()
    {
        return 10;
    }

    private void Awake()
    {
        _player = FindObjectOfType<Jugador>().transform;
        _nextFire = _fireRate;
        currentHealth = 500;
    }

	private void Update () {
		Attack();
	}

    private void Attack() {
        if (!(Time.time > _nextFire)) return;
        
        _nextFire = Time.time + _fireRate;

        var dir = transform.position.x - _player.position.x;
        
        var transformLocalScale = transform.localScale;
        
        transformLocalScale.x = dir < 0 ? 1 : -1;
        
        transform.localScale = transformLocalScale;
        
        myAnimator.SetTrigger("Attack");
    }
}
