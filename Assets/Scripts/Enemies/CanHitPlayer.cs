﻿public interface CanHitPlayer
{
   float GetDamage();
}
