﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class BaseEnemy : Enemy
{
    [SerializeField] private Collider2D attackCollider;
    [SerializeField] private HitTrigger hitTrigger;
    [SerializeField] private SpriteRenderer mySpriteRenderer;

    private bool _isAttacking;

    private float _distanceToPlayer, _movementDelay = 3, _nextMovement;

    private int _directionToPlayer, _walkingDirection;

    private Vector3 _originalPosition, _walkingTarget;
    
    private Coroutine _attackingCoroutine;

    protected override void SetStats()
    {
        currentHealth = 100;
        armor = 30;
        damage = 35;
    }

    public void EnableAttackCollider()
    {
        hitTrigger.CanHit();
        var x = _originalPosition.x * _directionToPlayer;
        var p = new Vector3(x, _originalPosition.y);
        attackCollider.transform.localPosition = p;
        _attackingCoroutine = StartCoroutine(MoveForward());
        attackCollider.enabled = true;
    }

    private IEnumerator MoveForward()
    {
        var t = transform.position + Vector3.right * (_directionToPlayer * 2);

        while (Mathf.Abs(t.x - transform.position.x) > 0.5f)
        {
            var d = Vector3.Lerp(transform.position, t, Time.deltaTime*5);
            myRigidbody.MovePosition(d);
            yield return new WaitForEndOfFrame();
        }

        _isAttacking = false;
        _attackingCoroutine = null;
    }

    public void DisableAttackCollider()
    {
        attackCollider.enabled = false;
        _isAttacking = false;
        
        if(_attackingCoroutine != null)
            StopCoroutine(_attackingCoroutine);
        
        _attackingCoroutine = null;
    } 

    private void Start()
    {
        _originalPosition = attackCollider.transform.localPosition;
        _walkingTarget = transform.position;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>();
        playerTransform = player.transform;
    }

    private void FixedUpdate()
    {
        var x = transform.position.x - playerTransform.position.x;
        _distanceToPlayer = Mathf.Abs(x);

        if (_distanceToPlayer > 4)
            WalkAround();
        else
        {
            if (_isAttacking) return;
            myAnimator.SetBool("Walking", false);
            Attack();
            _walkingTarget = transform.position;
        }

    }

    private void WalkAround()
    {
        if (_isAttacking) return; 
        
        var p = transform.position;
        if (Time.time > _nextMovement)
        {
            _walkingDirection = (int) Mathf.Sign(new Random().Next(-1, 1));
            _walkingTarget = p + Vector3.right * (5 * _walkingDirection);
            _nextMovement = Time.time + _movementDelay;
        }

        mySpriteRenderer.flipX = _walkingDirection > 0;
        myAnimator.SetBool("Walking", true);
        myRigidbody.position = Vector3.Lerp(myRigidbody.position, _walkingTarget, Time.fixedDeltaTime);
    }

    private void Attack()
    {
        _isAttacking = true;
        _directionToPlayer = (int) Mathf.Sign(playerTransform.position.x - transform.position.x);
        mySpriteRenderer.flipX = _directionToPlayer > 0;
        myAnimator.SetTrigger("Attack");
    }
}
