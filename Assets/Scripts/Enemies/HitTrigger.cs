﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTrigger : MonoBehaviour
{
    private CanHitPlayer _enemy;
    private bool _alreadyHit;

    public void CanHit() => _alreadyHit = false;
    

    private void Awake()
    {
        _enemy = transform.parent.GetComponent<CanHitPlayer>() ?? GetComponent<CanHitPlayer>();
    }

    private void OnDisable()
    {
        _alreadyHit = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        CompareAndHit(other);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        CompareAndHit(other);
    }

    private void CompareAndHit(Collider2D other)
    {
        if (!other.CompareTag("Player") || _alreadyHit) return;
        
        FindObjectOfType<Jugador>().ReceiveDamage(_enemy.GetDamage());
        _alreadyHit = true;
    }
}
