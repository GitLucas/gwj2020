﻿using UnityEngine;

public class FlyingEnemy : Enemy
{
    [SerializeField] private float attackSpeed;
    [SerializeField] private Collider2D attackCollider;
    [SerializeField] private HitTrigger hit;

    private bool _ableToAttack, _isAttacking;
    private Vector3 _originalPos, _target, _start;
    
    protected override void SetStats()
    {
        currentHealth = 50;
        armor = 20;
        damage = 20;
    }

    private float _baseY, _baseExtra, _attackingTime;

    private void Start()
    {
        _originalPos = transform.position;
        _baseY = _originalPos.y;
        _ableToAttack = true;
        player = FindObjectOfType<Jugador>();
        playerTransform = player.transform;
    }

    private void FixedUpdate()
    {
        if (!_isAttacking && (transform.position - playerTransform.position).magnitude > 6) 
            IdleMovement();
        else
        {
            if (_target == Vector3.zero)
            {
                _start = transform.position;
                _target = playerTransform.position;
                _attackingTime = 0;
            }
            Attack();
        }
    }

    private void IdleMovement()
    {
        _target = Vector3.zero;
        _ableToAttack = true;
        var v = transform.position;
        _baseExtra += Time.fixedDeltaTime / 2;
        v.y = _baseY + Mathf.Sin(_baseExtra);
        transform.position = v;
    }

    private void Attack()
    {
        _baseExtra = 0f;
        _isAttacking = true;
        if (!_ableToAttack)
        {
            MoveToOriginalPosition();
            return;
        }

        _attackingTime += Time.fixedDeltaTime * attackSpeed;
        var position1 = Vector3.Lerp(_start, _target, _attackingTime);
        transform.position = position1;
        var m = (position1 - _target).magnitude;
        _ableToAttack = m > 2.125;

        if (!_ableToAttack)
        {
            myAnimator.SetTrigger("Attack");
        }
    }

    public void EnableAttackCollider()
    {
        attackCollider.enabled = true;
    }
    
    public void DisableAttackCollider()
    {
        attackCollider.enabled = false;
        hit.CanHit();
    }

    private void MoveToOriginalPosition()
    {
        var v = transform.position;
        var newV = Vector2.Lerp(v, _originalPos, Time.deltaTime * (attackSpeed/4));
        
        if (Vector2.Distance(v, _originalPos) < .125f)
        {
            _ableToAttack = true;
            _isAttacking = false;
            _target = Vector3.zero;
        }
        
        transform.position = newV;
    }
}
