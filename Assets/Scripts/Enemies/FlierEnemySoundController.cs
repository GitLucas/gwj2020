﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlierEnemySoundController : EnemySoundController
{
    public override void SoundDeath() => SoundAPI.PostEvent(SoundEvent.FlierEnemyDeath, gameObject);
}
