﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HittableByPlayer : MonoBehaviour
{
    /*
     * class only made not to use tags but nullrefs
     */
    
    protected float currentHealth;
    protected float armor;
    protected float damage;
    
    public virtual void ReceiveDamage(int dmg) {
        var a = armor * 0.01f;
        var b = 1 - a;
        currentHealth -= dmg * b;
    }
}
