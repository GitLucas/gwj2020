﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundController : MonoBehaviour
{
    public void SoundAttack() => SoundAPI.PostEvent(SoundEvent.EnemyAttack, gameObject);
    public void SoundFootstep() => SoundAPI.PostEvent(SoundEvent.EnemyFootstep, gameObject);
    public void SoundHurt() => SoundAPI.PostEvent(SoundEvent.Hit, gameObject);
    public virtual void SoundDeath() => SoundAPI.PostEvent(SoundEvent.EnemyDeath, gameObject);
}
