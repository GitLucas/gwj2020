﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : HittableByPlayer, CanHitPlayer
{

    [SerializeField] protected Animator myAnimator;
    [SerializeField] protected Collider2D myCollider;
    [SerializeField] protected Floor level;
    [SerializeField] protected Rigidbody2D myRigidbody;

    protected Jugador player;
    protected Transform playerTransform;
    
    public float GetDamage() => damage;

    private void Awake () {
        SetStats();
	}

    protected virtual void SetStats()
    {
        currentHealth = 100;
        armor = 20;
        damage = 10;
    }

    public void DisableMyCollider()
    {
        myCollider.enabled = false;
        myRigidbody.isKinematic = true;
    }

    public void EnableMyCollider()
    {
        myCollider.enabled = true;
        myRigidbody.isKinematic = false;
    } 

    public void EnemyDeath()
    {
        level.KillEnemy();
	    Destroy(gameObject);
    }

    public override void ReceiveDamage(int dmg) {
        base.ReceiveDamage(dmg);
        
        DisableMyCollider();

        myAnimator.SetBool("Dead", currentHealth <= 0);
        myAnimator.SetTrigger("Hurt");
    }

    public void SetFloor(Floor f) => level = f;
}