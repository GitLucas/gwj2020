﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentSoundController : MonoBehaviour
{
    private static EnvironmentSoundController _instance;

    public static EnvironmentSoundController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<EnvironmentSoundController>();

            return _instance;
        }
    }
    
    private void Awake()
    {
        if (_instance)
        {
            Destroy(gameObject);
            return;
        }
        
        _instance = this;
        SoundAPI.SetState(SoundStateGroup.MusicState, SoundState.General);
        SoundAPI.SetState(SoundStateGroup.Player, SoundState.PlayerAlive);
    }

    private void Start() 
    {
        SoundAPI.PostEvent(SoundEvent.PlayMusic, gameObject);
    }
}
