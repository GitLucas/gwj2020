﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Floor : MonoBehaviour
{
    
    [SerializeField] protected Collider2D rightCollider;
    [SerializeField] protected Transform spawningPoint;
    [SerializeField] private bool isStarting;

    public event Action OnFirstLevelSurpassed;

    public Vector3 SpawnPosition => spawningPoint.position;

    protected int _enemyCount;
    protected LevelGenerator _lg;
    protected Floor _nextFloor;
    protected GameObject _cameraGo;

    protected virtual void Awake () {
        SpawnEnemies();
        _cameraGo = GameObject.FindGameObjectWithTag("MainCamera");
        _lg = _cameraGo.GetComponentInChildren<LevelGenerator>();
    }

    protected virtual void SpawnEnemies() {
        _enemyCount = 0;
        int j = 0;
        GameObject[] auxArray = new GameObject[10];

        for (var i = 0; i < transform.childCount; i++) {
            var child = transform.GetChild(i);
            if (child.CompareTag("Enemy"))
                auxArray[j++] = child.gameObject;
        }

        var rand = new System.Random();

        
        for (var i = 0; i < j; i++)
        {
            if (auxArray[i] == null) continue;

            var val = rand.Next(0, 2);
            if (val == 1)
                auxArray[i].SetActive(false);
            else
            {
                auxArray[i].GetComponent<Enemy>().SetFloor(this);
                _enemyCount++;
            }
        }

        if (isStarting || _enemyCount != 0) return;
        
        var ind = rand.Next(0, j);
        auxArray[ind].SetActive(true);
        auxArray[ind].GetComponent<Enemy>().SetFloor(this);
        _enemyCount++;
        
    }

    public void KillEnemy() {
        _enemyCount--;

        if (_enemyCount > 0) return;
        
        _nextFloor = _lg.EndLevel();
        _enemyCount = -1;
        rightCollider.isTrigger = true;
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;

        other.transform.position = _nextFloor.SpawnPosition;
        _cameraGo.transform.position += Vector3.up*10;
        gameObject.SetActive(false);
        
        if(isStarting) OnFirstLevelSurpassed?.Invoke();
    }
}
