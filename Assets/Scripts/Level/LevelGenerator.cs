﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {
    
    [Header("Environment")]
    [SerializeField] private GameObject[] floors;
    [SerializeField] private GameObject endingFloor;
    [SerializeField] private GameObject exitPoint;
    [SerializeField] private GameObject chest;

    [Header("Helpers")] 
    [SerializeField] private BuildingController buildingController;
    
    private int _floorCount, _currentFloor;
    private float _height;
    private System.Random r = new System.Random();

    private void Awake()
    {
        _floorCount = r.Next(10, 20);
        _currentFloor = 1;
        _height = 10; // Should be changed when actual floors are delivered
        
        buildingController.setHeight(_height);
    }
	
	private void Start () {
        //for(var i = 0; i < 10; i++)
            //SpawnNextFloor();
        
        //Instantiate( endingFloor, new Vector3( 0, _height * _floorCount, 0), Quaternion.identity );
	}

    private Floor SpawnNextFloor() {

        var randomObject = r.Next(0, floors.Length);
        var t = floors[randomObject].transform;
        var position = t.position;
        
        return Instantiate( floors[randomObject], 
            new Vector3( position.x, position.y + _height * _currentFloor++, position.z), 
            Quaternion.identity ).GetComponent<Floor>();
    }

    public Floor EndLevel() {

        if( _currentFloor < _floorCount) 
            return SpawnNextFloor();
        
        var position = endingFloor.transform.position;
        
        return Instantiate( endingFloor, 
            new Vector3( position.x, position.y + (_height * _floorCount), position.z ), 
            Quaternion.identity ).GetComponent<Floor>();
    }
}
