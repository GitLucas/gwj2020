﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class PlayerMovementController : MonoBehaviour {


    [SerializeField] private BoxCollider2D myCollider;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private SpriteRenderer mySpriteRenderer;
    [SerializeField] private Animator myAnimator;
    [SerializeField] private float movementSpeed = 25;
    [SerializeField] private float jumpForce = 75;

    public int Direction => mySpriteRenderer.flipX ? -1 : 1;

    public bool IsJumping { get; private set; }

    public bool IsAttacking { get; set; }

    private int _direction;
    
	void Start () {
        
	}
    
	void Update () {

        float media = myCollider.bounds.size.x / 2;
        var position = transform.position;
        var d = float.Parse((myCollider.bounds.size.y / 1.5).ToString());
        RaycastHit2D hitRight = Physics2D.Raycast( new Vector2(position.x + media, position.y), Vector2.down, d);
        RaycastHit2D hitLeft = Physics2D.Raycast( new Vector2(position.x - media, position.y), Vector2.down, d);
            
        //Debug.DrawRay(new Vector2(position.x + media, position.y), Vector2.down * d, Color.magenta);
        IsJumping = !(hitLeft.collider || hitRight.collider);
    }

    private void FixedUpdate()
    {
        if(IsAttacking) SlowDown();
        else Move(_direction);
    }
    
    public void PlayerDeath()
    {
        enabled = false;
        rb.isKinematic = true;
        myCollider.enabled = false;
    }

    public void OnMove(InputValue value)
    {
        _direction = (int) value.Get<Vector2>().x;
    }

    public void OnJump(InputValue value)
    {
        if (!IsJumping)
            Jump();
    }

    private void Move(int x)
    {
        if (x == 0)
        {
            myAnimator.SetBool("Walking", false);
            return;
        }
        
        var v = rb.velocity;
        var aux = new Vector3(v.x + x * movementSpeed * Time.fixedDeltaTime, v.y, 0);
        
        rb.velocity = aux;
        
        myAnimator.SetBool("Walking", true);
        
        mySpriteRenderer.flipX = x < 0;
    }

    private void SlowDown()
    {
        var v = rb.velocity;
        var newV = Vector3.Lerp(v, Vector3.zero, Time.fixedDeltaTime * (1/movementSpeed));
        rb.velocity = newV;
    }

    private void Jump() 
    {
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }

    public bool CanDoubleJump() => IsJumping;

    public float GetJumpForce() => jumpForce;
}
