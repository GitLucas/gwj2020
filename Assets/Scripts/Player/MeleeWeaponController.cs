﻿using UnityEngine;

namespace Player
{
    public class MeleeWeaponController : GenericWeaponController
    {
        [Header("Melee")]
        [SerializeField] private Collider2D hitCollider;

        [SerializeField] private Transform hitTransform;

        private Vector3 _originalPosition;

        public override void Start()
        {
            base.Start();
            _originalPosition = hitTransform.localPosition;
        }

        protected override void Attack()
        {
            var x = _originalPosition.x * playerMovement.Direction;
            var p = new Vector3(x, _originalPosition.y);
            hitTransform.transform.localPosition = p;
            //myAnimator.SetBool("Ground", !playerMovement.IsJumping);
            myAnimator.SetTrigger("Attack");

            playerMovement.IsAttacking = true;
        }

        public void EnableHitCollider() => hitCollider.enabled = true;
        public void DisableHitCollider() => hitCollider.enabled = false;
    }
}
