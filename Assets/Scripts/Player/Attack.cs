﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    [SerializeField] private Jugador player;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var h = other.GetComponent<HittableByPlayer>();
        if (!h) return;
        h.ReceiveDamage(player.Damage);
    }

    public GameObject GetShoot() {
        return null;
    }
}
