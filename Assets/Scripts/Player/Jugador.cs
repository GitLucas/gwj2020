﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Jugador : MonoBehaviour
{
    public event Action OnHealthLost;

    [SerializeField] private Animator myAnimator;
    [SerializeField] private Rigidbody2D myRigidBody;
    [SerializeField] private Collider2D myCollider;
    [SerializeField] private PlayerInput playerInput;

    private bool _hittable;

    // Atributos del personaje
    private int _maxHealth, _currentHealth;
    private float _armor; 
    private int _damage;

    public int Damage => _damage;

    private void Awake() {
        _maxHealth = 3;
        _currentHealth = _maxHealth;
        _armor = 20f;
        _damage = 45;
        _hittable = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        var h = collision.gameObject.GetComponent<CanHitPlayer>();

        if(h != null)
            ReceiveDamage(h.GetDamage());

    }

    public void ReceiveDamage(float enemyD) 
    {
        AuxReceiveDamage();
        return;
        
        var x = Math.Truncate(enemyD * (1 - _armor / 100));
        _currentHealth -= (int) x;
    }

    private void AuxReceiveDamage()
    {
        if (!_hittable) return;
        
        _currentHealth--;
        OnHealthLost?.Invoke();
        
        if (_currentHealth <= 0)
        { 
            myAnimator.SetBool("Dead", true);
            myRigidBody.velocity = Vector2.zero;
            playerInput.enabled = false;
            myRigidBody.isKinematic = true;
            myCollider.enabled = false;
        }
        else 
            StartCoroutine(DamageCoroutine());
        
        myAnimator.SetTrigger("Hurt");
    }

    private IEnumerator DamageCoroutine()
    {
        myRigidBody.isKinematic = true;
        myCollider.enabled = false;
        _hittable = false;
        var v = myRigidBody.velocity;
        myRigidBody.velocity = Vector2.zero;
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(.125f);
        Time.timeScale = .5f;
        yield return new WaitForSecondsRealtime(.25f);
        Time.timeScale = 1;
        myRigidBody.velocity = v;
        myRigidBody.isKinematic = false;
        myCollider.enabled = true;
        yield return new WaitForSecondsRealtime(.625f);
        _hittable = true;
    }

    public void BackToMenu() => SceneManager.LoadScene(0);

    public void AumentarVida(int plus) {
        _maxHealth += plus;
        _currentHealth += plus;
    }

    public void AumentarDanio(int plus) {
        _damage += plus;
    }

    public void AumentarResistencia(float plus) {
        _armor += plus;
    }
}
