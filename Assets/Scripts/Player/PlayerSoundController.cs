﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundController : MonoBehaviour
{
    private void Awake()
    {
        SoundAPI.SetState(SoundStateGroup.Player, SoundState.PlayerAlive);
    }

    public void SoundFootstep() => SoundAPI.PostEvent(SoundEvent.PlayerFootstep, gameObject);

    public void SoundDeath()
    {
        SoundAPI.SetState(SoundStateGroup.Player, SoundState.PlayerDead);
        SoundAPI.PostEvent(SoundEvent.PlayerDeath, gameObject);
    }

    public void SoundHurt() => SoundAPI.PostEvent(SoundEvent.Hit, gameObject);

    public void SoundSword() => SoundAPI.PostEvent(SoundEvent.Attack, gameObject);

}
