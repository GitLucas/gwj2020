﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class shitytester : MonoBehaviour
{
    public void MoveVertically(float dir)
    {
        transform.position += dir * 10 * Vector3.up;
    }
    
    public void OnMove(InputValue value)
    {
        var y = value.Get<Vector2>().y;
        
        if(Math.Abs(y) > .001f) MoveVertically(y);
    }
}
