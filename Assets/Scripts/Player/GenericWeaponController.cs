﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class GenericWeaponController : MonoBehaviour {

    private Vector3 offset;
    [SerializeField] protected Arma arma;
    [SerializeField] protected Animator myAnimator;
    [SerializeField] protected PlayerMovementController playerMovement;

	// Use this for initialization
	public virtual void Start () {
		//offset = transform.position - transform.parent.position;
	}
	
	// Update is called once per frame
	public virtual void Update () {
		//Translate();
        //Attack();
	}
    
    public void SetAttackingFalse()
    {
        playerMovement.IsAttacking = false;
    }
    
    public void OnFire(InputValue value)
    {
        Attack();
    }

    protected abstract void Attack();

    void Translate() {

        if ( Input.GetAxis("Vertical") > 0 ) 
            this.transform.position = this.transform.parent.transform.position + Vector3.up;
        else if ( Input.GetAxis("Vertical") < 0 )
            this.transform.position = this.transform.parent.transform.position - Vector3.up;
        else
            this.transform.position += offset;
        

    }

    public Arma getWeapon() {
        return arma;
    }
}
