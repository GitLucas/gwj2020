/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ATTACK = 180661997U;
        static const AkUniqueID ENEMY_ATTACK = 1781417190U;
        static const AkUniqueID ENEMY_DEAD = 3805217580U;
        static const AkUniqueID ENEMY_FLY_DEAD = 2240500274U;
        static const AkUniqueID ENEMY_FOOTSTEPS = 3217092839U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID FX_SECRET_ROOM_PLAY = 1768088025U;
        static const AkUniqueID FX_SECRET_ROOM_STOP = 105658911U;
        static const AkUniqueID HIT = 1116398592U;
        static const AkUniqueID MUSIC_PLAY = 202194903U;
        static const AkUniqueID PLAYER_DEAD = 2226114503U;
        static const AkUniqueID SPIKE = 2812137U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSICSTATE
        {
            static const AkUniqueID GROUP = 1021618141U;

            namespace STATE
            {
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID DEAD = 2044049779U;
                static const AkUniqueID GENERAL = 133642231U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID SECRETROOM = 1348541312U;
            } // namespace STATE
        } // namespace MUSICSTATE

        namespace PLAYER
        {
            static const AkUniqueID GROUP = 1069431850U;

            namespace STATE
            {
                static const AkUniqueID ALIVE = 655265632U;
                static const AkUniqueID DEAD = 2044049779U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace PLAYER

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID LEVEL1 = 2678230382U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
